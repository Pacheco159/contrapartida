from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Estagiario,quant,anual
from .forms import EstagioForm

@login_required
def estagio_lista(request):
    estagios = Estagiario.objects.all()
    return render(request, 'estagio.html', {'estagios': estagios})

@login_required
def estagio_create(request):
    form = EstagioForm(request.POST, request.FILES or None)

    if form.is_valid():
        form.save()
        return redirect('estagiolista')

    return render(request, 'estagioform.html', {'form': form})

@login_required
def estagio_update(request, id):
    estagio = get_object_or_404(Estagiario, pk=id)
    form = EstagioForm(request.POST or None, request.FILES or None, instance=estagio)

    if form.is_valid():
        form.save()
        return redirect('estagiolista')

    return render(request, 'estagioupdate.html', {'form': form})

@login_required
def estagio_delete(request, id):
    estagio = get_object_or_404(Estagiario, pk=id)
    form = EstagioForm(request.POST or None, request.FILES or None, instance=estagio)

    if request.method == 'POST':
        estagio.delete()
        return redirect('estagiolista')

    return render(request, 'estagiodelete.html', {'form':form})

@login_required
def dash_board(request):
    estagios = Estagiario.objects.all()
    return render(request, 'dashboard.html', {'estagios': estagios, 'q':quant(),'a':anual()})




