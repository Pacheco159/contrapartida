from django.db import models
from convenio.models import Convenio

TURNO_CHOICES = [
    ('M', 'Manhã'),
    ('T', 'Tarde'),
    ('N', 'Noite'),
]

class Estagiario(models.Model):
    Disciplina = models.CharField(max_length=80, unique=True, null=False, blank=False)
    Curso = models.CharField(max_length=30, blank=False, null=False)
    Turno = models.CharField(choices=TURNO_CHOICES, null=False, blank=False, max_length=50)
    Preceptor = models.CharField(max_length=30, blank=False, null=False)
    Quantidade = models.IntegerField(blank=False, null=False)
    Custo_por_Disciplina = models.DecimalField(decimal_places=2, max_digits=10, blank=False, null=False)

    def __str__(self):
        return self.Disciplina


def quant():
    v = 0
    for i in Estagiario.objects.all():
        c = i.Quantidade
        v += c
    return v


def anual():
    v = 0
    for i in Estagiario.objects.all():
        c = i.Custo_por_Disciplina
        v += c
    return v * quant()





