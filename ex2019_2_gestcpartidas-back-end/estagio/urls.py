from django.urls import path
from .views import estagio_lista, estagio_create, estagio_update, estagio_delete, dash_board

urlpatterns = [
    path('lista/', estagio_lista, name="estagiolista"),
    path('create/', estagio_create, name="estagiocreate"),
    path('update/<int:id>/', estagio_update, name="estagioupdate"),
    path('delete/<int:id>/', estagio_delete, name="estagiodelete"),
    path('board/', dash_board, name="dashboard"),

]