from django.forms import ModelForm
from .models import Estagiario


class EstagioForm(ModelForm):
    class Meta:
        model = Estagiario
        fields = ['Disciplina', 'Curso', 'Preceptor', 'Turno', 'Quantidade', 'Custo_por_Disciplina']

    def __init__(self, *args, **kwargs):
        super(EstagioForm, self).__init__(*args, **kwargs)
        self.fields['Disciplina'].widget.attrs['placeholder'] = 'Disciplina'
        self.fields['Curso'].widget.attrs['placeholder'] = 'Curso'
        self.fields['Preceptor'].widget.attrs['placeholder'] = 'Preceptor'
        self.fields['Turno'].widget.attrs['placeholder'] = 'Turno'
        self.fields['Quantidade'].widget.attrs['placeholder'] = 'Quantidade de Estagiarios'
        self.fields['Custo_por_Disciplina'].widget.attrs['placeholder'] = 'R$'
