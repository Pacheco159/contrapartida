from django.forms import ModelForm
from .models import FormRelatorio

class RelatorioForm(ModelForm):
    class Meta:
        model = FormRelatorio
        fields = ['Nome', 'Matricula', 'Disciplina', 'CPF', 'Curso']