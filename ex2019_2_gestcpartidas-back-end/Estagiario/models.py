from django.db import models

class FormRelatorio(models.Model):
    Nome = models.CharField(max_length=50,null=False, blank=False)
    Matricula = models.CharField(max_length=50,null=False, blank=False)
    Disciplina = models.CharField(max_length=50,null=False, blank=False)
    CPF = models.CharField(max_length=50,null=False, blank=False)
    Curso = models.CharField(max_length=50,null=False, blank=False)

    def __str__(self):
        return self.Nome

