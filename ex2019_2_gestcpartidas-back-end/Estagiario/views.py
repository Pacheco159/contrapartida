from django.shortcuts import render, redirect, get_object_or_404
from .models import FormRelatorio
from .forms import RelatorioForm
from django.contrib.auth.decorators import login_required


@login_required

def EstagiarioList(request):
    estagiarios = FormRelatorio.objects.all()

    return render(request, 'estagiarioList.html', {'estagiarios': estagiarios})

@login_required
def EstagiarioCreat(request):
    form = RelatorioForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('estagiarioList')

    return render(request, 'estagiarioCreate.html', {'form': form})

def EstagiarioUpdate(request, id):
    estagiario = get_object_or_404(FormRelatorio, pk=id)
    form = RelatorioForm(request.POST or None, instance = estagiario)

    if form.is_valid():
        form.save()
        return redirect('estagiarioList')

    return render(request, 'estagiarioCreate.html', {'form': form})

def EstagiarioDelete(request, id):
    estagiario = get_object_or_404(FormRelatorio, pk=id)

    if request.method == 'POST':
        estagiario.delete()
        return redirect('estagiarioList')

    return render(request, 'estagiarioDelete.html', {'estagiario': estagiario})

