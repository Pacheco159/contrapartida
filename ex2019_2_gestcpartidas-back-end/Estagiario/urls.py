from django.urls import path
from .views import EstagiarioList, EstagiarioCreat, EstagiarioUpdate, EstagiarioDelete

urlpatterns = [
    path('list/', EstagiarioList, name='estagiarioList'),
    path('create/', EstagiarioCreat, name='estagiarioCreate'),
    path('update/<int:id>/', EstagiarioUpdate, name='estagiarioUpdate'),
    path('delete/<int:id>/', EstagiarioDelete, name='estagiarioDelete'),

]
