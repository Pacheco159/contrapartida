from django.apps import AppConfig


class EstagiarioConfig(AppConfig):
    name = 'Estagiario'
