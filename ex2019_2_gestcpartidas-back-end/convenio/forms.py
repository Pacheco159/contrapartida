from django.forms import ModelForm
from .models import Convenio


class ConvenioForm(ModelForm):
    class Meta:
        model = Convenio
        fields = ['Convenios', 'Preceptor', 'Distritos', 'Instituição',
                  'Endereço', 'Supervisor_de_Estágio', 'Estagiários']

    def __init__(self, *args, **kwargs):
        super(ConvenioForm, self).__init__(*args, **kwargs)
        self.fields['Preceptor'].widget.attrs['placeholder'] = 'Preceptor'
        self.fields['Instituição'].widget.attrs['placeholder'] = 'Instituição'
        self.fields['Endereço'].widget.attrs['placeholder'] = 'Endereço'
        self.fields['Supervisor_de_Estágio'].widget.attrs['placeholder'] = 'Supervisor de Estágio'
        self.fields['Estagiários'].widget.attrs['placeholder'] = 'Quantidade de Estagiários'

