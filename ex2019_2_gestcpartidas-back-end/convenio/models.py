from django.db import models

CONVENIO_CHOICES = [
    ('Estado', 'Estado'),
    ('Município', 'Município'),
]

DISTRITO_CHOICES = [
    ('A', 'Distrito1'),
    ('B', 'Distrito2'),
    ('C', 'Distrito3'),
    ('D', 'Distrito4'),
    ('E', 'Distrito5'),
]


class Convenio(models.Model):
    Convenios = models.CharField(choices=CONVENIO_CHOICES, null=False, max_length=30)
    Distritos = models.CharField(choices=DISTRITO_CHOICES, null=False, max_length=50)
    Instituição = models.CharField(max_length=80, blank=False, null=False)
    Endereço = models.CharField(max_length=50, blank=False, null=False)
    Supervisor_de_Estágio = models.CharField(max_length=30, blank=False, null=False)
    Preceptor = models.CharField(max_length=30, blank=False, null=False)
    Estagiários = models.CharField(max_length=30, blank=False, null=False)
    


    def __str__(self):
        return self.Convenios