from django.shortcuts import render, redirect, get_object_or_404
from .models import Convenio
from .forms import ConvenioForm
from django.contrib.auth.decorators import login_required


# aqui estão todas as funções de um CRUD




@login_required
def convenio_lista(request):
    busca = request.GET.get('pesquisa', None)

    if busca:
        convenios = Convenio.objects.all()
        convenios = convenios.filter(Supervisor_de_Estágio=busca)
    else:
        convenios = Convenio.objects.all()
    return render(request, 'convenio.html', {'convenios': convenios})


@login_required
def convenio_create(request):
    form = ConvenioForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        return redirect('conveniolista')
    return render(request, 'convenioform.html', {'form': form})


@login_required
def convenio_update(request, id):
    convenio = get_object_or_404(Convenio, pk=id)
    form = ConvenioForm(request.POST or None, request.FILES or None, instance=convenio)

    if form.is_valid():
        form.save()
        return redirect('conveniolista')

    return render(request, 'convenioupdate.html', {'form': form})


@login_required
def convenio_delete(request, id):
    convenio = get_object_or_404(Convenio, pk=id)
    form = ConvenioForm(request.POST or None, request.FILES or None, instance=convenio)

    if request.method == 'POST':
        convenio.delete()
        return redirect('conveniolista')

    return render(request, 'conveniodelete.html', {'form': form})
