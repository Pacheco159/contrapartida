from django.urls import path
from .views import convenio_lista, convenio_create, convenio_update, convenio_delete

urlpatterns = [
    path('lista/', convenio_lista, name="conveniolista"),
    path('create/', convenio_create, name="conveniocreate"),
    path('update/<int:id>/', convenio_update, name="convenioupdate"),
    path('delete/<int:id>/', convenio_delete, name="conveniodelete"),
]