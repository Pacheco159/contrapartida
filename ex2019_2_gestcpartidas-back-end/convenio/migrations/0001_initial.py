# Generated by Django 2.2.6 on 2019-10-31 21:47

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Convenio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Convenios', models.CharField(choices=[('Estado', 'Estado'), ('Município', 'Município')], max_length=30)),
                ('Distritos', models.CharField(choices=[('A', 'Distrito1'), ('B', 'Distrito2'), ('C', 'Distrito3'), ('D', 'Distrito4'), ('E', 'Distrito5')], max_length=50)),
                ('Instituição', models.CharField(max_length=80)),
                ('Endereço', models.CharField(max_length=50)),
                ('Supervisor_de_Estágio', models.CharField(max_length=30)),
                ('Preceptor', models.CharField(max_length=30)),
                ('Estagiários', models.CharField(max_length=30)),
            ],
        ),
    ]
